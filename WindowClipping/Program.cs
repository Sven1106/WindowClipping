﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.Windows.Forms;

namespace WindowClipping
{
    class Program
    {

        #region Import Window Functions From DLL
        [DllImport("User32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int maxCount);

        [DllImport("user32.dll")]
        private static extern int GetWindowTextLength(IntPtr hWnd);

        #region Mouse Cursor
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hwnd, out Rectangle lpRect);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool ClipCursor(ref Rectangle rcClip);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetCursorPos(ref Win32Point pt);


        [StructLayout(LayoutKind.Sequential)]
        internal struct Win32Point
        {

            public Int32 X;
            public Int32 Y;
        };

        public static Point GetMousePosition()
        {
            Win32Point w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);
            return new Point(w32Mouse.X, w32Mouse.Y);
        }

        private const int MF_BYCOMMAND = 0x00000000;
        public const int SC_CLOSE = 0xF060;
        public const int SC_MINIMIZE = 0xF020;
        public const int SC_MAXIMIZE = 0xF030;
        public const int SC_SIZE = 0xF000;

        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);

        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();
        #endregion

        #endregion


        /// <summary> 
        /// Copies the text of the specified window's title bar (if it has one) into a buffer.  
        /// </summary>
        /// <param name="hWnd"> A handle to the window or control containing the text.</param>
        public static string GetWindowText(IntPtr hWnd)
        {
            int size = GetWindowTextLength(hWnd);
            if (size > 0)
            {
                var builder = new StringBuilder(size + 1);
                GetWindowText(hWnd, builder, builder.Capacity);//How does GetWindowText write to Builder?
                return builder.ToString();
            }
            return String.Empty;
        }


        static void Main()
        {
            IntPtr handle = GetConsoleWindow();
            IntPtr sysMenu = GetSystemMenu(handle, false);


            if (handle != IntPtr.Zero)
            {
                // DeleteMenu(sysMenu, SC_CLOSE, MF_BYCOMMAND);
                //DeleteMenu(sysMenu, SC_MINIMIZE, MF_BYCOMMAND);
                DeleteMenu(sysMenu, SC_MAXIMIZE, MF_BYCOMMAND);
                DeleteMenu(sysMenu, SC_SIZE, MF_BYCOMMAND);
            }

            int origWidth;
            int width;
            int origHeight;
            int height;
            origWidth = Console.WindowWidth;
            origHeight = Console.WindowHeight;
            width = origWidth / 2;
            height = origHeight / 4 + 3;
            Console.SetWindowSize(width, height);
            Console.SetBufferSize(width, height);


            Rectangle primaryScreen = Screen.PrimaryScreen.Bounds;
            Point centerPointOfScreen = new Point(primaryScreen.Width / 2, primaryScreen.Height / 2);
            int timeout = 100;
            while (true)
            {
                Console.SetCursorPosition(0, 0);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("         ______      ____\n");
                Console.Write("        / ____/_  __/ / /  __________________  ___  ____\n");
                Console.Write("       / /_  / / / / / /  / ___/ ___/ ___/ _ \\/ _ \\/ __ \\\n");
                Console.Write("      / __/ / /_/ / / /  (__  ) /__/ /  /  __/  __/ / / /\n");


                IntPtr foregroundWindowAddress = GetForegroundWindow();
                var windowTitle = GetWindowText(foregroundWindowAddress);

                GetWindowRect(foregroundWindowAddress, out Rectangle foregroundWindow);//based on the ForegroundWindow
                ConsoleColor foregroundLockColor;
                if (primaryScreen.Width == foregroundWindow.Width && primaryScreen.Height == foregroundWindow.Height && windowTitle != "")
                {
                    Point cursorPos = GetMousePosition();
                    int xDistanceFromCenterToCursor = Math.Abs(centerPointOfScreen.X - cursorPos.X);
                    int yDistanceFromCenterToCursor = Math.Abs(centerPointOfScreen.Y - cursorPos.Y);
                    int xDistanceFromCenterToCursorInPercentage = (int)((decimal)xDistanceFromCenterToCursor / centerPointOfScreen.X * 100);
                    int yDistanceFromCenterToCursorInPercentage = (int)((decimal)yDistanceFromCenterToCursor / centerPointOfScreen.Y * 100);
                    int highestPercentageDistanceFromCenter = Math.Max(xDistanceFromCenterToCursorInPercentage, yDistanceFromCenterToCursorInPercentage);
                    timeout = 100 - (highestPercentageDistanceFromCenter <= 100 ? highestPercentageDistanceFromCenter : 100);
                    ClipCursor(ref foregroundWindow);
                    foregroundLockColor = ConsoleColor.DarkGreen;
                }
                else
                {
                    foregroundLockColor = ConsoleColor.DarkRed;
                    timeout = 100;
                }

                Console.Write("   __/_/_   \\__,_/_/_/  /____/\\___/_/  "); ConsoleWriteColor("_", foregroundLockColor);Console.Write("\\___"); Console.Write("/\\___/_/");ConsoleWriteColor("_", foregroundLockColor); Console.Write("/_/\n"); 
                Console.Write("  / ____/_  ________________  _____   "); ConsoleWriteColor("/ /___  _____/ /__\n", foregroundLockColor);
                Console.Write(" / /   / / / / ___/ ___/ __ \\/ ___/  "); ConsoleWriteColor("/ / __ \\/ ___/ //_/\n", foregroundLockColor);
                Console.Write("/ /___/ /_/ / /  (__  ) /_/ / /     "); ConsoleWriteColor("/ / /_/ / /__/ ,<\n", foregroundLockColor);
                Console.Write("\\____/\\__,_/_/  /____/\\____/_/     "); ConsoleWriteColor("/_/\\____/\\___/_/|_|\n", foregroundLockColor);
                Thread.Sleep(timeout);

            }

            void ConsoleWriteColor(string text, ConsoleColor color)
            {
                ConsoleColor oldColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.Write(text);
                Console.ForegroundColor = oldColor;
            }
        }

    }
}
